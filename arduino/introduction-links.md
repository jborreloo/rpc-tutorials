#Introduction to Arduino

The arduino has become one of the most ubiquitous electronics prototyping boards in the world and has spawned a whole market full of similar products (many of which can be programmed and controlled in exactly the same way. The following links provide access to tutorials put together by arduino to introduce you to some of the key concepts behind the board and the software used to program it.


To begin with, if you haven't downloaded the arduino software already, you can find it [here](http://arduino.cc/download)

The tutorials linked below increase with complexity as you move down the page. They also have a short description about what information they contain.

###All of the following links are from subsections of the [Arduino Foundations Page](https://www.arduino.cc/en/Tutorial/Foundations)

* For the *very* beginning introduction to arduino, use [this link](https://www.arduino.cc/en/Guide/Introduction)

* If you don't really care about the philosophy behind arduino but want to understand how the board is laid out, go [here](https://www.arduino.cc/en/Guide/BoardAnatomy)
   * After this tutorial, you might want to look at these links for more specific information on:
   * [Digital Pins](https://www.arduino.cc/en/Tutorial/DigitalPins)
   * [Analog Pins](https://www.arduino.cc/en/Tutorial/AnalogInputPins)
   * [PWM - aka, Analog Write](https://www.arduino.cc/en/Tutorial/PWM)
   * and if you're feeling very advanced, you can even learn more about how the arduino deals with [memory](https://www.arduino.cc/en/Tutorial/Memory)

* To learn more about the software you program arduinos in, check out [this link](https://www.arduino.cc/en/Guide/Environment) to learn about the actual software environment and [this one](https://www.arduino.cc/en/Tutorial/Sketch) to learn about how to write a program, called a Sketch, in that environment.
   * Once you've gotten a hang of how the software works, check out these links to learn more:
   * [Language Reference](https://www.arduino.cc/en/Reference/HomePage), to learn about the meanings of each term in the arduino code
   * [Variables](https://www.arduino.cc/en/Tutorial/Variables)
   * [Functions](https://www.arduino.cc/en/Reference/FunctionDeclaration)
   * [Using Libraries](https://www.arduino.cc/en/Guide/Libraries), for when the built-in capabilities of the arduino software don't quite cut it
   * And if you're feeling really ambitious, check out [Cores](https://www.arduino.cc/en/Guide/Cores) to learn how to add your own board to the arduino software

The arduino is a very powerful and extendible tool and as you become more and more well-versed in it, you will find there are tons of opportunities to [hack](https://www.arduino.cc/en/Hacking/HomePage) around the arduino, with either its hardware or software.
