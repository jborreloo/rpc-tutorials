# Hacking Together a Capacitive Sensor

### An RPC Hackathon Tutorial

*Over the past few decades, capacitive sensing has become a nearly ubiquitous technology. It is the enabling technology in most touch screen devices, including tablets and cell phones and has a variety of sensor applications for research and applied settings. Capacitive sensing can be used to determine distance, position, electrical properties, and event triggering, to list a few applications that have already been conceived. While we often associate capacitive sensors with expensive, high-technology equipment such as tablets, hacking together a simple capacitive sensor can be one of the quickest and most inexpensive prototyping techniques you can acheive. This tutorial will walk you through the basics of how to make your own capacitive sensor, including all the equipment and software you'll need to get going.*

## What you'll need 
* **Hardware:**
    * [Arduino Uno](https://www.adafruit.com/products/50) (or similar boards, i.e. - Arduino Mega or Arduino Micro)
    * [hookup wire](http://sfe.io/p8867) (solid core is preferred, but any wire will work)
    * [tape](http://sfe.io/p10561) (electrical or copper is best, but again you can make other adhesives work)
    * tin/aluminum foil (or any other conductive material)
    * *Optional: [100k-ohm resistor](http://sfe.io/p10969), or higher*
* **Software:**
    * Arduino IDE, downloaded [here](https://www.arduino.cc/en/Main/Software)
    * Arduino Capacitive Sensing library, downloaded [here](http://playground.arduino.cc/Main/CapacitiveSensor?from=Main.CapSense)

## Getting started
* **Installing Arduino IDE**
    * The Arduino software is downloaded as an app and can be run as soon as it is downloaded
* **Installing the Capacitive Sensing libraries**
    * Download the .zip file from the arduino website *(link provided above)*
    * Unzip the folder
    * The folder will need to be moved to the **libraries** folder in your local arduino directory (on Windows this is usually in `C://Program Files(x86)/Arduino/libraries`, for Mac this is usually in `Documents/Arduino/libraries`)
      * It is important that the folder name does not have any unauthorized characters (such as -, /, ?)
      * Typically it is best to rename the folder 'Library_Name'
    * *It should also be noted that in latest versions of the Arduino IDE, there is a library package manager included (top bar: Sketch-->Include Library-->Manage Libraries...) which can be used to search for and install the CapacitiveSensor library*
    * Once the library is downloaded and installed, you should be able to runt the CapacitiveSensor example from the Examples folder (top bar: File-->Examples-->CapacitiveSensor-->CapacitiveSensorSketch)

## Making the sensor
* For preliminary purposes, the example program will more than suffice, so don't worry about messing around with the code for now (unless you really want to)
* To make the sensor, take a piece of foil and attach two strands of wire to either side of it
    * Connect one of the wires to pin 4 on the arduino
    * Connect the other wire to the resistor (if using one) and then connect the resistor to pin 2, either directly or with another piece of wire
    * ![wiring](https://bytebucket.org/jborreloo/rpc-tutorials/raw/84058e874c0eb590535106f650e91bd304228c5e/capsense/capsense_wiring_diagram_bb.jpg "wiring diagram")

## Running the Program
* Now run the arduino program by pressing the upload button (arrow on top menu) or **Ctrl+U**

![upload](https://bytebucket.org/jborreloo/rpc-tutorials/raw/252e07bfd6e9265a9c75152a8278c8bf0588053c/arduino_menu_bar-upload.png "arduino upload button")

* To see the sensor output, open the Serial Monitor (top bar: Tools-->Serial Monitor, top menu: magnifying glass icon, keystrokes: **Ctrl+Shift+M**)

![serial](https://bytebucket.org/jborreloo/rpc-tutorials/raw/252e07bfd6e9265a9c75152a8278c8bf0588053c/arduino_menu_bar-serial.png "arduino serial monitor button")

* The values you will see are the capacitive sensor readings for the sensor you've just set up
    * *there will also be readings for two other sensors that you did not set up and these values will not be changing as much if at all - the example code comes set up for three simultaneous sensor readings. If you want to follow the example code exactly there are instructions in the code as well oas on the CapacitiveSensor library download page.*
    * To see the values change, try bringing your hand closer and further away from the foil. This is effectively a crude distance sensor and various thresholds can be set for the sensor values in order to trigger other events.

## Expanding the project
If you want to extend the power of your new capcitive sensor and incorporate it into other projects, you can try exploring some of these options:

* Use multiple capacitive sensors (the example is set up for a maximum of 3) to create an array that senses distance and position
* Use the proximity sensing capabilities of the sensor to turn on or off an LED light when you bring your hand within a certain distance
* Use multiple sensors plus an RGB LED light to create an RGB color "blender" that mixes different levels of each color depending on the reading from each sensor
