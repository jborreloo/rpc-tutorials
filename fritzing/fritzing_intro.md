# Intro to circuit design with Fritzing
### An RPC Hackathon Workshop Tutorial

*Fritzing is a simple circuit design software often used in conjunction with arduino. It is capable of producing breadboard, schematic, and PCB layouts and can even be used to directly place an order for a circuit board you've designed. Often in rapid prototyping, fritzing is used to record a circuit layout once you've reached a design you like using a breadboard, jumper wires and through-hole components. For this reason, it's breadboard layout is especially useful when working with rapid prototyping electronics equipment, since you can replicate exactly what you see on screen.*

##Downloading fritzing
* Just like the [arduino](http://arduino.cc/download) software, fritzing is downloaded as an app and can be run from the directory it is downloaded to.
* The frizting download link and OS-specific operating instructions can be found [here](http://fritzing.org/download)

##Fritzing view modes
* When you first launch fritzing, it will open in **breadboard view**. This view displays circuits and components in the same way they would look if they were sitting in front of you on your bench. Breadboards will look like breadboards, LED lights will look like LED lights, etc. Unless you change the software setting, this will be the viewing mode fritzing will always open in.
* The second view mode is **schematic view**. This view represents the circuit using conventional electrical engineering symbols. If you were to include a diagram of a circuit in a technical paper or manuscript, this is the view you would use.
* The third view mode is **PCB view**. This view represents the circuit as a PCB (printed circuit board) trace, which is how any electronic board you may have seen or worked with in the past is made. Unless you're planning on having a custom circuit of your electronic design made, you won't really be working in this view.

* *the majority of this tutorial will focus on using fritzing in breadboard view and may deal a little bit with the schematic view, but otherwise these other two view modes will not be covered. Future tutorials will be made that deal more in depth with schematic and PCB view.*

##Breadboard Layout
